from django.contrib import admin
from .models import Camera, Comment


class CameraAdmin(admin.ModelAdmin):
    list_display = ('identifier', 'name')  # Mostrar identificador y nombre en la lista de cámaras


class CommentAdmin(admin.ModelAdmin):
    list_display = ('camera', 'timestamp', 'text')  # Mostrar cámara, fecha y texto en la lista de comentarios
    list_filter = ('camera',)  # Filtrar comentarios por cámara


# Registro los modelos camera y comment
admin.site.register(Camera, CameraAdmin)
admin.site.register(Comment, CommentAdmin)
